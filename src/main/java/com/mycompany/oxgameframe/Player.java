/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxgameframe;

/**
 *
 * @author User
 */
public class Player {
    private char name;
    private int win;
    private int lose;
    private int draw;

    public Player(char c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public void Win() {
        win++;
    }

    public int getLose() {
        return lose;
    }

    public void Lose() {
        lose++;
    }

    public int getDraw() {
        return draw;
    }

    public void Draw() {
        draw++;
    }

}
